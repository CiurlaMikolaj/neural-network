const brain = require('brain.js');
const _ = require('lodash');
const fs = require('fs');

const net = new brain.NeuralNetwork({ 
  activation: 'sigmoid',
  hiddenLayers: [10][10] 
});

const numTrainingData = 1048575;
const numTestData = 1000000;

const trainData = readFile('taxi-fare-train.csv');
const testData = readFile('taxi-fare-test.csv');

const slicedTrainData = sliceData(trainData, numTrainingData);
const slicedTestData = sliceData(testData, numTestData);
  
console.log('done training', net.train(slicedTrainData));

let error1 = 0;
let error2 = 0;
let error3 = 0;
let tmp = 0;
for (let i = 0; i < numTestData; ++i) {
  const { fare_amount } = net.run(_.omit(testData[i], ['fare_amount']));
  error1 += Math.abs(fare_amount - testData[i].fare_amount);
  error2 += Math.abs(fare_amount - testData[i].fare_amount)/testData[i].fare_amount;
  tmp = Math.abs(fare_amount - testData[i].fare_amount)/testData[i].fare_amount;
  if(tmp<0.1) {error3++;}
  //console.log(i, '\tpredicted: ' + fare_amount.toFixed(5), '\treal: ' + testData[i].fare_amount, '\taccuracy [%]: ' + (fare_amount/testData[i].fare_amount*100).toFixed(0) + '%' );
}

console.log('Average error1', (error1 / numTestData).toFixed(5));
console.log('Average error2', (error2*100 / numTestData).toFixed(2) + '%');
console.log('Average error3', (error3 / numTestData).toFixed(3));

console.log('done');



function readFile(path) {
  const raw = fs.readFileSync(path, 'utf8').split('\n');
  const headers = raw[0].split(',').map(header => header.replace(/"/g, ''));

  const data = raw.
  slice(1).
  map(line => line.split(',').
  reduce((cur, v, i) => {
    if (headers[i].includes('time')) {
      cur[headers[i]] = parseFloat(v) / 10000;
    } else if (headers[i].includes('fare')) {
      cur[headers[i]] = parseFloat(v) / 100;
    } else if (headers[i].includes('vendor') || headers[i].includes('payment')){
    } else {
      cur[headers[i]] = parseFloat(v) / 10;
    }
    return cur;
  }, {}));

  return data;
}

function sliceData(data, amount){
  const slicedData = data.
  slice(0, amount).
  map(obj => ({
    input: _.omit(obj, ['fare_amount']),
    output: _.pick(obj, ['fare_amount'])
  }));

  return slicedData
}